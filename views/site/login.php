<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-info">Для входа введите <strong>admin/admin</strong></div>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
    ]); ?>

    <?= $form->field($model, 'username')
        ->textInput()
        ->label('Имя пользователя')
    ?>

    <?= $form->field($model, 'password')
        ->passwordInput(['autofocus' => true])
        ->label('Пароль')
    ?>

    <?= $form->field($model, 'rememberMe')
        ->checkbox()
        ->label('Запомнить меня')
    ?>

    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-9">
            <button type="submit" class="btn btn-primary">Войти</button>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
