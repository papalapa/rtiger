<?php

use app\models\Room;
use app\models\RoomBookingForm;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\MaskedInput;

/* @var $this View */
/* @var $room Room */
/* @var $model RoomBookingForm */
/* @var $success bool */

$this->title = "Заказ комнаты «‎{$room->name}»‎";

?>
<div class="site-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'id' => 'room-booking-form',
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tel')->widget(MaskedInput::class, [
        'mask' => '+7(999)999-99-99',
    ]) ?>

    <?= $form->field($model, 'start')->widget(DatePicker::class, [
        'options' => ['data-datepicker' => '', 'placeholder' => 'Выберите дату заселения'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true,
            'startDate' => date('Y-m-d'),
        ],
    ]) ?>

    <?= $form->field($model, 'for_one_day')->checkbox(['data-for-one-day' => '']) ?>

    <div data-for-one-day-target="">
        <?= $form->field($model, 'end')->widget(DatePicker::class, [
            'options' => ['data-datepicker' => '', 'placeholder' => 'Выберите дату выезда'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true,
                'startDate' => date('Y-m-d'),
            ],
        ]) ?>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Заказать</button>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    window.onload = function () {
        $('[data-for-one-day]').on('input', function () {
            const isChecked = $(this).is(':checked');
            if (isChecked) {
                $('[data-for-one-day-target]').hide();
            } else {
                $('[data-for-one-day-target]').show();
            }
        }).trigger('input');
    }
</script>
