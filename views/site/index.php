<?php

use app\models\Room;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ListView;

/* @var $this View */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Главная';
?>
<div class="site-index">

    <h1>Номера для заказа</h1>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => static function (Room $model, $key, $index, $widget) {
            return implode(PHP_EOL, [
                Html::beginTag('div', ['class' => 'panel panel-default']),
                Html::beginTag('div', ['class' => 'panel-heading']),
                Html::tag('h3', $model->name, ['class' => 'panel-title']),
                Html::endTag('div'),
                Html::beginTag('div', ['class' => 'panel-body']),
                Html::tag('p', $model->description),
                Html::endTag('div'),
                Html::beginTag('div', ['class' => 'panel-footer text-right']),
                Html::a('Заказать', ['site/try-to-book', 'id' => $model->id], ['class' => 'btn btn-info']),
                Html::endTag('div'),
                Html::endTag('div'),
            ]);
        },
    ]) ?>

</div>
