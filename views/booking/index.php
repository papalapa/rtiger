<?php

use app\components\RoomHelper;
use app\models\Booking;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы на номера';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Html::a('+ Создать новый', ['create'], ['class' => 'btn btn-success']) ?></p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'room_id',
                'filter' => RoomHelper::dropdownList(),
                'value' => static function (Booking $model) {
                    return $model->room->name;
                },
            ],
            'name',
            'tel',
            'start',
            'end',
            'datetime',

            ['class' => \yii\grid\ActionColumn::class],
        ],
    ]) ?>

    <?php Pjax::end(); ?>

</div>
