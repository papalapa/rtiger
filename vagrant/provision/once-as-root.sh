#!/usr/bin/env bash

#== Bash helpers ==

function info {
  echo " "
  echo "--> $1"
  echo " "
}

#== Provision script ==

export DEBIAN_FRONTEND=noninteractive

info "Provision-script user: `whoami`"

info "Create database 'dev'"
mysql -uroot <<< "CREATE DATABASE IF NOT EXISTS dev"
echo "Done!"

info "Create database user 'admin' with password 'password'"
mysql -uroot <<< "CREATE USER admin@'%' IDENTIFIED BY 'password'"
mysql -uroot <<< "GRANT ALL PRIVILEGES ON *.* TO admin@'%'"
echo "Done!"

info "Flushing privileges"
mysql -uroot <<< "FLUSH PRIVILEGES"
echo "Done!"

info "Set PHP-CLI version to 7.2"
sudo update-alternatives --set php /usr/bin/php7.2
echo "Done!"

info "Enabling sites configuration for nginx"
sudo ln -s /app/vagrant/nginx/app.conf /etc/nginx/sites-enabled/app.conf
echo "Done!"
