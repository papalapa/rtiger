#!/usr/bin/env bash

#== Import script args ==

github_token=$(echo "$1")

#== Bash helpers ==

function info {
  echo " "
  echo "--> $1"
  echo " "
}

#== Provision script ==

info "Provision-script user: `whoami`"

info "Configure composer"
composer config --global github-oauth.github.com ${github_token}
composer global require ocramius/package-versions
echo "Done!"

info "Install project dependencies"
cd /app
composer --no-progress --prefer-dist install

php yii migrate --interactive=0
