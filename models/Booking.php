<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\BaseActiveRecord;

/**
 * This is the model class for table "booking".
 *
 * @property int    $id
 * @property int    $room_id
 * @property string $name
 * @property string $tel
 * @property string $start
 * @property string $end
 * @property string $datetime
 *
 * @property Room   $room
 */
class Booking extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'room_id' => 'Room ID',
            'name' => 'Name',
            'tel' => 'Tel',
            'start' => 'Start',
            'end' => 'End',
            'datetime' => 'Datetime',
        ];
    }

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        return [
            'TimestampBehavior' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    BaseActiveRecord::EVENT_BEFORE_INSERT => ['datetime'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['room_id', 'name', 'tel', 'start', 'end'], 'required'],

            [['name'], 'string', 'max' => 128],
            [['tel'], 'string', 'max' => 16],

            [['start', 'end'], 'date', 'format' => 'php:Y-m-d'],

            [['room_id'], 'integer'],
            [['room_id'], 'exist', 'targetClass' => Room::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * Gets query for [[Room]].
     *
     * @return \yii\db\ActiveQuery|RoomQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Room::class, ['id' => 'room_id']);
    }

    /**
     * {@inheritdoc}
     * @return BookingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BookingQuery(static::class);
    }
}
