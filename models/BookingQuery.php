<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Booking]].
 *
 * @see Booking
 */
class BookingQuery extends \yii\db\ActiveQuery
{
    /**
     * @param Room $room
     *
     * @return BookingQuery
     */
    public function forRoom(Room $room)
    {
        return $this->andWhere(['room_id' => $room->id]);
    }

    /**
     * @param $start
     * @param $end
     *
     * @return BookingQuery
     */
    public function busy($start, $end)
    {
        return $this->andWhere([
            'OR',
            ['AND', ['<=', 'start', $start], ['>=', 'end', $end]],
            ['AND', ['>=', 'start', $start], ['<=', 'end', $end]],
            ['BETWEEN', 'start', $start, $end],
            ['BETWEEN', 'end', $start, $end],
        ]);
    }

    /**
     * {@inheritdoc}
     * @return Booking[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Booking|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
