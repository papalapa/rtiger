<?php

namespace app\models;

use app\components\BaseSearchModel;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;

/**
 * Class RoomSearch
 *
 * @package app\models
 */
class RoomSearch extends BaseSearchModel
{
    public $id;

    public $name;

    public $description;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'description'], 'safe'],
        ];
    }

    /**
     * @param array $params
     *
     * @return DataProviderInterface
     */
    public function search($params) : DataProviderInterface
    {
        $query = Room::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
