<?php

namespace app\models;

use app\components\BaseSearchModel;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;

/**
 * Class BookingSearch
 *
 * @package app\models
 */
class BookingSearch extends BaseSearchModel
{
    public $id;

    public $room_id;

    public $name;

    public $tel;

    public $start;

    public $end;

    public $datetime;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'room_id'], 'integer'],
            [['name', 'tel', 'start', 'end', 'datetime'], 'safe'],
        ];
    }

    /**
     * @param array $params
     *
     * @return DataProviderInterface
     */
    public function search($params) : DataProviderInterface
    {
        $query = Booking::find()->with('room');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'room_id' => $this->room_id,
            'start' => $this->start,
            'end' => $this->end,
            'datetime' => $this->datetime,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'tel', $this->tel]);

        return $dataProvider;
    }
}
