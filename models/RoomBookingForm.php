<?php

namespace app\models;

use yii\base\Model;
use yii\validators\Validator;

/**
 * Class RoomBookingForm
 *
 * @package app\models
 */
class RoomBookingForm extends Model
{
    public $name;

    public $tel;

    public $start;

    public $end;

    public $for_one_day = false;

    /**
     * @var Room
     */
    private $room;

    /**
     * RoomBookingForm constructor.
     *
     * @param Room  $room
     * @param array $config
     */
    public function __construct(Room $room, $config = [])
    {
        parent::__construct($config);

        $this->room = $room;
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Ваше имя',
            'tel' => 'Ваш номер телефона',
            'start' => 'Дата заселения',
            'end' => 'Дата выезда',
            'for_one_day' => 'На один день',
        ];
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 128],

            [['tel'], 'required'],
            [['tel'], 'string', 'max' => 16],
            [
                ['tel'], 'match', 'pattern' => '/^\+7\(\d{3}\)\d{3}\-\d{2}\-\d{2}$/',
                'message' => 'Номер телефона должен быть указан в формате +7(XXX)XXX-XX-XX',
            ],

            [['for_one_day'], 'boolean'],

            [['start'], 'required'],
            [['start'], 'date', 'format' => 'php:Y-m-d'],
            [['start'], 'compare', 'compareValue' => date('Y-m-d'), 'operator' => '>='],

            [
                ['end'], 'required',
                'when' => function () {
                    return !$this->for_one_day;
                },
                'enableClientValidation' => false,
            ],
            [
                ['end'], 'filter',
                'filter' => function () {
                    return $this->end = $this->for_one_day ? $this->start : $this->end;
                },
                'when' => function () {
                    return !$this->hasErrors('start');
                },
            ],
            [['end'], 'date', 'format' => 'php:Y-m-d'],
            [
                ['end'], 'compare',
                'compareAttribute' => 'start',
                'operator' => '>=',
                'message' => 'Дата выезда должна быть позже даты заселения',
                'enableClientValidation' => false,
            ],
            [['start', 'end'], 'validateFree'],
        ];
    }

    /**
     * @param           $attribute
     * @param           $params
     * @param Validator $validator
     */
    public function validateFree($attribute, $params, Validator $validator)
    {
        if (!$this->hasErrors()) {
            $roomIsBusy = Booking::find()->forRoom($this->room)->busy($this->start, $this->end)->exists();

            if ($roomIsBusy) {
                $message = 'Не удалось забронировать номер, так как в этот период он частично занят другим клиентом';
                $this->addError($attribute, $message);
            }
        }
    }

    /**
     * @return Booking
     */
    public function makeInstance() : Booking
    {
        $booking = new Booking(['room_id' => $this->room->id]);
        $booking->name = $this->name;
        $booking->tel = $this->tel;
        $booking->start = $this->start;
        $booking->end = $this->end;

        return $booking;
    }

    /**
     * @return void
     */
    public function reset() : void
    {
        $this->setAttributes([
            'name' => null,
            'tel' => null,
            'start' => null,
            'end' => null,
        ]);
    }
}
