<?php

namespace app\components;

use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

/**
 * Class ManageController
 *
 * @package app\components
 */
abstract class ManageController extends SecureController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = $this->getSearchModelClass();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', compact('model'));
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = $this->getModelClass();

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->primaryKey]);
        }

        return $this->render('create', compact('model'));
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->primaryKey]);
        }

        return $this->render('update', compact('model'));
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModel($id) : ActiveRecord
    {
        $modelClass = $this->getModelClass();
        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Страница не найдена');
    }

    /**
     * @return ActiveRecord
     */
    abstract protected function getModelClass() : ActiveRecord;

    /**
     * @return BaseSearchModel
     */
    abstract protected function getSearchModelClass() : BaseSearchModel;
}
