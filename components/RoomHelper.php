<?php

namespace app\components;

use app\models\Room;
use yii\helpers\ArrayHelper;

/**
 * Class RoomHelper
 *
 * @package app\components
 */
class RoomHelper
{
    /**
     * @return array
     */
    public static function dropdownList() : array
    {
        $rooms = Room::find()->all();

        return ArrayHelper::map($rooms, 'id', 'name');
    }
}
