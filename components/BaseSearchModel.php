<?php

namespace app\components;

use yii\base\Model;
use yii\data\DataProviderInterface;

/**
 * Class BaseSearchModel
 *
 * @package app\components
 */
abstract class BaseSearchModel extends Model
{
    /**
     * @param array $params
     *
     * @return DataProviderInterface
     */
    abstract public function search(array $params) : DataProviderInterface;
}
