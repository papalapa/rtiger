<?php

namespace app\commands;

use app\models\Room;
use Faker\Provider\ru_RU\Address;
use Faker\Provider\ru_RU\Company;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Class FakeNumbersController
 *
 * @package app\commands
 */
class FakeRoomController extends Controller
{
    /**
     * @var string
     */
    public $defaultAction = 'init';

    /**
     * @param int $count
     *
     * @return int
     */
    public function actionInit(int $count)
    {
        echo "Ok, попробуем сгенерировать новые комнаты в количестве {$count} шт.".PHP_EOL;

        for ($i = 0; $i < $count; $i++) {
            $name = sprintf('%s-%s', Address::buildingNumber(), Address::street());
            $description = sprintf('%s %s %s', Address::streetPrefix(), Address::country(), Company::companyNameElement());

            $room = new Room(compact('name', 'description'));
            if ($room->save()) {
                echo '.';
            }
        }

        echo PHP_EOL;

        echo 'Готово!'.PHP_EOL;

        return ExitCode::OK;
    }
}
