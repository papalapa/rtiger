<?php

require __DIR__.'/../vendor/autoload.php';

$dotenv = new \Symfony\Component\Dotenv\Dotenv();
$dotenv->loadEnv(__DIR__.'/../.env');

defined('YII_DEBUG') or define('YII_DEBUG', $_ENV['APP_ENV'] === 'dev');
defined('YII_ENV') or define('YII_ENV', $_ENV['APP_ENV']);

require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = yii\helpers\ArrayHelper::merge(
    require __DIR__.'/../config/main.php',
    require __DIR__.'/../config/web.php'
);
$config['params'] = array_diff_key($_ENV, ['SYMFONY_DOTENV_VARS' => false]);

(new yii\web\Application($config))->run();
