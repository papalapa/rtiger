<?php

namespace app\controllers;

use app\components\BaseSearchModel;
use app\components\ManageController;
use app\models\Room;
use app\models\RoomSearch;
use yii\db\ActiveRecord;

/**
 * Class RoomController
 *
 * @method Room findModel($id)
 *
 * @package app\controllers
 */
class RoomController extends ManageController
{
    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $room = $this->findModel($id);

        if (count($room->bookings)) {
            \Yii::$app->session->addFlash('warning', 'Нельзя удалить комнату, так как есть заказы');

            return $this->redirect('index');
        }

        $room->delete();

        return $this->redirect(['index']);
    }

    /**
     * @return ActiveRecord
     */
    protected function getModelClass() : ActiveRecord
    {
        return new Room();
    }

    /**
     * @return BaseSearchModel
     */
    protected function getSearchModelClass() : BaseSearchModel
    {
        return new RoomSearch();
    }
}
