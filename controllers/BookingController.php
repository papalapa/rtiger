<?php

namespace app\controllers;

use app\components\BaseSearchModel;
use app\components\ManageController;
use app\models\Booking;
use app\models\BookingSearch;
use yii\db\ActiveRecord;

/**
 * Class BookingController
 *
 * @package app\controllers
 */
class BookingController extends ManageController
{
    /**
     * @return ActiveRecord
     */
    protected function getModelClass() : ActiveRecord
    {
        return new Booking();
    }

    /**
     * @return BaseSearchModel
     */
    protected function getSearchModelClass() : BaseSearchModel
    {
        return new BookingSearch();
    }
}
