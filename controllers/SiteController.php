<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\Room;
use app\models\RoomBookingForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider();
        $dataProvider->query = Room::find()->orderBy(['name' => SORT_ASC]);
        $dataProvider->pagination->pageSize = 5;
        $dataProvider->pagination->pageSizeParam = false;

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionTryToBook($id)
    {
        $room = $this->findRoom($id);

        $success = false;
        $model = new RoomBookingForm($room);

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $booking = $model->makeInstance();
                if ($booking->save()) {
                    $message = sprintf('Поздравляем! Номер зарегистрирован на имя %s, дата заселения - %s, дата выезда - %s', $booking->name,
                        $booking->start, $booking->end);
                    Yii::$app->session->addFlash('success', $message);
                    $model->reset();
                } else {
                    foreach ($booking->firstErrors as $firstError) {
                        Yii::$app->session->addFlash('error', $firstError);
                    }
                }
            } else {
                foreach ($model->firstErrors as $firstError) {
                    Yii::$app->session->addFlash('error', $firstError);
                }
            }
        }

        return $this->render('try-to-book', compact('room', 'model', 'success'));
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            $rememberMeTime = $model->rememberMe ? Yii::$app->params['REMEMBER_ME_TIME'] : 0;
            Yii::$app->user->login($user, $rememberMeTime);

            return $this->goBack();
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @param $id
     *
     * @return Room
     * @throws NotFoundHttpException
     */
    private function findRoom($id) : Room
    {
        $room = Room::findOne($id);
        if ($room === null) {
            throw new NotFoundHttpException('Номер не найден');
        }

        return $room;
    }
}
