<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%room}}`.
 */
class m200417_040754_create_room_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('room', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull()->unique(),
            'description' => $this->text()->notNull(),
        ]);

        $this->alterColumn('room', 'id', $this->integer()->unsigned()->notNull().' AUTO_INCREMENT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('room');
    }
}
