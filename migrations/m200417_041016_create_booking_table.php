<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%booking}}`.
 */
class m200417_041016_create_booking_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('booking', [
            'id' => $this->primaryKey(),
            'room_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string(128)->notNull(),
            'tel' => $this->string(16)->notNull(),
            'start' => $this->date()->notNull(),
            'end' => $this->date()->notNull(),
            'datetime' => $this->timestamp()->notNull(),
        ]);

        $this->alterColumn('booking', 'id', $this->integer()->unsigned()->notNull().' AUTO_INCREMENT');
        $this->addForeignKey('fk__booking-room_id__room-id', 'booking', 'room_id', 'room', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk__booking-room_id__room-id', 'booking');
        $this->dropTable('booking');
    }
}
